<?php

use Illuminate\Support\Facades\Route;

Route::get('docs/{version}', ['as' => 'docs.index', 'uses' => 'DocsController@index']);
Route::get('docs/{version}/yaml', ['as' => 'docs.file', 'uses' => 'DocsController@getDocsFile']);
