<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'elements', 'middleware' => 'throttle:1'], function () {
    Route::get('/', 'ElementController@index');
    Route::post('/', 'ElementController@store');
    Route::patch('/', 'ElementController@update');
    Route::delete('/', 'ElementController@delete');
});
