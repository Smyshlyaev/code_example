<?php

namespace Tests\Unit;

use App\Element;
use App\Http\Requests\ElementStoreRequest;
use App\Repositories\ElementRepository;
use Faker\Factory as Faker;
use Tests\TestCase;

class UserTest extends TestCase
{
    private $elementRepository;

    private $elementStoreRequest;

    private $element;

    public function setUp(): void
    {
        parent::setUp();
        $this->elementRepository = new ElementRepository();
        $this->elementStoreRequest = new ElementStoreRequest();
        $this->element = new Element();
        $this->faker = Faker::create();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testElementRepositoryStore()
    {
        $title = $this->faker->name;
        $this->elementStoreRequest->offsetSet('title', $title);

        $element = $this->elementRepository->store($this->elementStoreRequest);

        $this->assertTrue($element instanceof $this->element);
        $this->assertSame($title, $element->title);
    }
}
