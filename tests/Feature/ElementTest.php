<?php

namespace Tests\Feature;

use App\Element;
use Faker\Factory as Faker;
use Tests\TestCase;

class ElementTest extends TestCase
{
    private $element;

    public function setUp(): void
    {
        parent::setUp();

        $this->element = new Element();
        $this->faker = Faker::create();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPositiveGetElementsTest()
    {
        $response = $this->get('/elements');
        $response->assertStatus(200);
    }

    public function test404()
    {
        $response = $this->get('/wrong');
        $response->assertStatus(404);
    }

    public function testGetElement()
    {
        $title = $this->faker->name;
        $this->element->truncate();
        $this->post('/elements', ["title" => $title]);
        $response = $this->get('/elements');
        $response->assertStatus(200)
            ->assertJson(
                [
                    ['title' => $title],
                ]
            );
    }

    public function testUpdateElement()
    {
        $firstTitle = $this->faker->name;
        $secondTitle = $this->faker->name;
        $this->element->truncate();
        $this->post('/elements', ["title" => $firstTitle]);

        $this->patch(
            '/elements',
            [
                "id" => Element::first()->id,
                "title" => $secondTitle,
            ]
        );

        $response = $this->get('/elements');
        $response->assertStatus(200)
            ->assertJson(
                [
                    ['title' => $secondTitle],
                ]
            );
    }

    public function testDeleteElement()
    {
        $title = $this->faker->name;
        $this->element->truncate();
        $this->post('/elements', ["title" => $title]);
        $this->delete(
            '/elements',
            [
                "id" => Element::first()->id,
            ]
        );
        $this->assertSame(Element::all()->count(), 0);
    }

}
