<?php

namespace App\Repositories;

use App\Element;
use App\Http\Requests\ElementDeleteRequest;
use App\Http\Requests\ElementStoreRequest;
use App\Http\Requests\ElementUpdateRequest;

class ElementRepository
{
    private $element;

    /**
     * ElementRepository constructor.
     */
    public function __construct()
    {
        $this->element = new Element();
    }

    /**
     * @return Element[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return $this->element->all();
    }

    /**
     * @param ElementStoreRequest $request
     * @return Element
     */
    public function store(ElementStoreRequest $request)
    {
        $element = $this->element;
        $element->title = $request->title;
        $element->save();

        return $element;
    }

    /**
     * @param ElementUpdateRequest $request
     * @return Element
     */
    public function update(ElementUpdateRequest $request)
    {
        $this->element->whereId($request->id)->update($request->all());
        return $this->element->find($request->id);
    }

    /**
     * @param ElementDeleteRequest $request
     * @return Element
     */
    public function delete(ElementDeleteRequest $request)
    {
        $this->element->whereId($request->id)->delete();

        return true;
    }
}
