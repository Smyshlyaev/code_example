<?php

namespace App\Providers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($value) {

            return Response::json($value);
        });

        Response::macro('error', function ($message) {

            $response = json_encode([
                'error' => $message
            ]);
            echo $response;
            die();
        });

        Response::macro('delete', function ($value) {

            return response('', 204)
                ->header('Content-Type', 'application/json');
        });
    }
}
