<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DocsController
 * @package App\Http\Controllers
 */
class DocsController extends Controller
{
    /**
     * @param string $version
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($version)
    {
        return view('docs', compact('version'));
    }

    /**
     * @param string $version
     * @return Response
     */
    public function getDocsFile($version)
    {
        $yaml = file_get_contents(base_path("docs/api/$version.yaml"));

        if (!$yaml) {
            throw new NotFoundHttpException();
        }

        return new Response($yaml, 200, ['Content-Type' => 'text/yaml']);
    }
}
