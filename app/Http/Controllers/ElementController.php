<?php

namespace App\Http\Controllers;

use App\Http\Requests\ElementDeleteRequest;
use App\Http\Requests\ElementIndexRequest;
use App\Http\Requests\ElementStoreRequest;
use App\Http\Requests\ElementUpdateRequest;
use App\Repositories\ElementRepository;


class ElementController extends Controller
{
    private $repository;

    public function __construct(ElementRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ElementIndexRequest $request
     * @return mixed
     */
    public function index(ElementIndexRequest $request)
    {
        $element = $this->repository->index();
        return response()->api($element);
    }

    /**
     * @param ElementStoreRequest $request
     * @return mixed
     */
    public function store(ElementStoreRequest $request)
    {
        $element = $this->repository->store($request);

        return response()->api($element);
    }

    /**
     * @param ElementUpdateRequest $request
     * @return mixed
     */
    public function update(ElementUpdateRequest $request)
    {
        $element = $this->repository->update($request);

        return response()->api($element);
    }

    /**
     * @param ElementDeleteRequest $request
     * @return mixed
     */
    public function delete(ElementDeleteRequest $request)
    {
        $this->repository->delete($request);

        return response('', 204)
            ->header('Content-Type', 'application/json');
    }
}
