<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ElementUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => ['required', 'max:255'],
            'id' => 'required|exists:elements,id',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
