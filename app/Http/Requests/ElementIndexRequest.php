<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ElementIndexRequest extends FormRequest
{
    public function rules()
    {
        return [
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
