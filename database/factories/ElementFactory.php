<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Element;
use Faker\Generator as Faker;

$factory->define(Element::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
    ];
});
