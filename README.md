# Вводная

Данный код является частью портфолио Смышляева Алексея. Этот код не имеет отношения к реальным проектам.

# Запуск контейнеров

./docker-up.sh

# Остановка контейнеров

./docker-down.sh

# Последовательность команд для запуска проекта на локали

cd /project 

cp .env.example .env 

cp .env.example .env.testing 

Заполните переменные окружения в файлах .env и .env-testing

composer install

php artisan key:generate

php artisan serve

# Запуск документации по следующему адресу

http://localhost:8000/docs/v1

# Запуск тестов

./vendor/bin/phpunit





